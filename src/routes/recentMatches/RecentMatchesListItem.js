import React from 'react'
import { MatchCard } from '../../components'

export const RecentMatchesListItem = ({ username, team, kills, deaths, accuracy, result }) => {
  scoreData = [
    {
      title: 'kills',
      value: kills
    },
    {
      title: 'deaths',
      value: deaths
    },
    {
      title: 'accuracy',
      value: Number(accuracy).toFixed(2).replace(/^0+/, '')
    }
  ]

  return (
    <MatchCard>
      <MatchCard.Header>
        <MatchCard.Title title={username} />
        <MatchCard.Subtitle subtitle={team} style={{ marginTop: 5 }} />
        <MatchCard.ResultBox result={result} />
      </MatchCard.Header>
      <MatchCard.Scores
        scores={scoreData}
      />
    </MatchCard>
  )
}
