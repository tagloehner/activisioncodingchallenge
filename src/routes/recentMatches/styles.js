import { StyleSheet } from 'react-native'

export const listStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#282828'
  },
  listContent: {
    padding: 14
  }
})

export const itemSeparatorStyle = StyleSheet.create({
  container: {
    height: 14,
  }
})