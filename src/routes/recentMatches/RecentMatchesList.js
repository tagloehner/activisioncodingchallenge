import React from 'react'
import { FlatList } from 'react-native'

import { RecentMatchesListItem } from './RecentMatchesListItem'
import { RecentMatchesListItemSeparator } from './RecentMatchesListItemSeparator'
import { LoadingView } from '../../components'
import { useFetch } from '../../hooks'
import { listStyles as styles } from './styles'
import { API_URL, COOKIE } from '../../constants'


export const RecentMatchesList = () => {
  const res = useFetch(API_URL, { headers: { Cookie: COOKIE } })
  if (!res.response) { return <LoadingView /> }
  return (
    <FlatList
      style={styles.container}
      data={res.response.data.matches}
      contentContainerStyle={styles.listContent}
      ItemSeparatorComponent={() => <RecentMatchesListItemSeparator />}
      keyExtractor={item => item.matchID}
      renderItem={({ item: { player: { username, team }, playerStats: { kills, deaths, accuracy }, result } }) => (
        <RecentMatchesListItem
          username={username}
          team={team}
          kills={kills}
          deaths={deaths}
          accuracy={accuracy}
          result={result}
        />
      )}
    />
  )
}
