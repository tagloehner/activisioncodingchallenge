import React from 'react'
import { View } from 'react-native'
import { itemSeparatorStyle as styles } from './styles'

export const RecentMatchesListItemSeparator = () => <View style={styles.container} />
