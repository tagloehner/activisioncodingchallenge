import React from 'react'
import { Text } from 'react-native'
import { styles } from './styles'

export const Title = ({ title }) => (
  <Text style={styles.text}> {title.toUpperCase()}</Text >
)
