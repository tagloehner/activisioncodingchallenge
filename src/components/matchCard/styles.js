
import { StyleSheet } from 'react-native'

export const matchCardStyle = StyleSheet.create({
  container: {
    flex: 1,
    height: 160,
    backgroundColor: '#111111',
    shadowColor: '#000000',
    shadowOpacity: 0.25,
    shadowRadius: 2,
  }
})
