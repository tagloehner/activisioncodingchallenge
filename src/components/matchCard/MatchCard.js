import React from 'react'
import { View } from 'react-native'
import { matchCardStyle } from './styles'
import { Title } from './title'
import { Header } from './header'
import { Subtitle } from './subtitle'
import { ResultBox } from './resultBox'
import { Scores } from './scores'

export const MatchCard = ({ style, children }) => {
  const { container } = matchCardStyle
  return (
    <View style={{ ...container, ...style }}>{children}</View >
  )
}

MatchCard.Title = Title
MatchCard.Header = Header
MatchCard.Subtitle = Subtitle
MatchCard.ResultBox = ResultBox
MatchCard.Scores = Scores