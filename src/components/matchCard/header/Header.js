import React from 'react'
import { View } from 'react-native'
import { styles } from './styles'

export const Header = ({ children }) => (
  <View style={styles.container}>{children}</View >
)

