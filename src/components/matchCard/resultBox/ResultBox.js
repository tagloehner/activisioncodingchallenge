import React from 'react'
import { View, Text } from 'react-native'
import { styles } from './styles'

export const ResultBox = ({ result }) => (
  <View style={{ ...styles.container, backgroundColor: result === 'win' ? '#9E903E' : '#333333' }}>
    <Text style={styles.text}>{result === 'win' ? 'W' : 'L'}</Text>
  </View>
)
