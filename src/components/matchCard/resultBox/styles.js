
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    height: 40,
    aspectRatio: 1.2,
    position: 'absolute',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20
  }
})
