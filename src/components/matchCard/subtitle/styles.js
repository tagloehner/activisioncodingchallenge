
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  text: {
    color: '#9E903E',
    fontWeight: 'bold',
    fontSize: 18
  }
})
