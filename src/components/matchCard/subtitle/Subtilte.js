import React from 'react'
import { Text } from 'react-native'
import { styles } from './styles'

export const Subtitle = ({ subtitle }) => (
  <Text style={styles.text}> {subtitle.toUpperCase()}</Text >
)
