import React, { Fragment } from 'react'
import { View, Text } from 'react-native'
import { styles } from './styles'

export const Scores = ({ scores }) => (
  <View style={styles.mainContainer}>
    {scores.map(score => {
      const { title, value } = score
      return (
        <Fragment key={title}>
          {value !== 'NaN' && <View style={styles.container}>
            <Text style={styles.title}>{title.toUpperCase()}</Text>
            <Text style={styles.value}>{value}</Text>
          </View>}
        </Fragment>
      )
    }
    )}
  </View>
)
