
import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  container: {
    aspectRatio: 1.5,
    padding: 10,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  title: {
    fontSize: 18,
    color: '#FFFFFF',
    fontWeight: '500'
  },
  value: {
    fontSize: 25,
    color: '#9E903E',
    fontWeight: '500'
  }
})
