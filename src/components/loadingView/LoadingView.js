import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import { styles } from './styles'

export const LoadingView = () => (
  <View style={styles.container}>
    <ActivityIndicator size="large" color="#FFFFFF" />
  </View>
)