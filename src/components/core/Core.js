import React, { Fragment } from 'react'
import { StatusBar } from 'react-native'
import { AppNavigator } from '../../navigator'

export const Core = () => (
  <Fragment>
    <StatusBar barStyle="light-content" />
    <AppNavigator />
  </Fragment>
)