import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { RecentMatchesList } from '../routes'

const AppNavigator = createStackNavigator({
  RecentMatchesList: {
    screen: RecentMatchesList,
    navigationOptions: () => ({
      title: 'RECENT MATCHES',
      headerStyle: {
        backgroundColor: '#000000',
        shadowRadius: 0,
        shadowOffset: {
          height: 0,
        },
      },
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerTitleAlign: 'center'
    }),
  }
})

export default createAppContainer(AppNavigator)